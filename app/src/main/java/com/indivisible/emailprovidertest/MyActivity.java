package com.indivisible.emailprovidertest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MyActivity
        extends ActionBarActivity
        implements View.OnClickListener, DialogInterface.OnClickListener
{


    ///////////////////////////////////////////////////////
    ////    data
    ///////////////////////////////////////////////////////

    // ui
    private EditText etUserEmail;
    private Button bAppendEmailProvider;
    private TextView tvUserEmailResult;
    private Button bConfirmEmail;

    // static
    private static String[] emailProviders;
    private static final String TAG = "emailProvider";


    ///////////////////////////////////////////////////////
    ////    init
    ///////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        ensureEmailProvidersArrayLoaded();
        initUI();
    }

    private void ensureEmailProvidersArrayLoaded()
    {
        if (emailProviders == null)
        {
            emailProviders = getResources().getStringArray(R.array.email_providers);
            Log.v(TAG, "(loadProviders) Number of providers loaded: " + emailProviders.length);
            for (int i = 0; i < emailProviders.length; i++)
            {
                Log.v(TAG, "(loadProviders) Provider " + i + ": " + emailProviders[i]);
            }
        }
    }

    private void initUI()
    {
        // views
        etUserEmail = (EditText) findViewById(R.id.login_EditText_userEmail);
        bAppendEmailProvider = (Button) findViewById(R.id.login_Button_emailProviders);
        tvUserEmailResult = (TextView) findViewById(R.id.login_TextView_resultEmail);
        bConfirmEmail = (Button) findViewById(R.id.login_Button_confirm);

        // clicks
        bAppendEmailProvider.setOnClickListener(this);
        bConfirmEmail.setOnClickListener(this);
    }


    ///////////////////////////////////////////////////////
    ////    button handling
    ///////////////////////////////////////////////////////

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.login_Button_emailProviders:
                displayDialog();
                break;
            case R.id.login_Button_confirm:
                tvUserEmailResult.setText("'" + etUserEmail.getText().toString() + "'");
                break;
        }
    }


    ///////////////////////////////////////////////////////
    ////    dialog
    ///////////////////////////////////////////////////////

    private void displayDialog()
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getString(R.string.dialog_emailProvider_title));

        ArrayAdapter dialogArrayAdapter = new ArrayAdapter(this,
                android.R.layout.select_dialog_item);
        for (String provider : emailProviders)
        {
            dialogArrayAdapter.add(provider);
        }
        dialogBuilder.setAdapter(dialogArrayAdapter, this);
        dialogBuilder.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which)
    {
        dialog.dismiss();
        emailProviderSelected(which);
    }

    protected void emailProviderSelected(int emailProviderIndex)
    {
        Log.i(TAG, "(emailProviderSelected) selected provider: "
                + emailProviders[emailProviderIndex]);
        String newEmailText = appendEmailProvider(etUserEmail.getText().toString().trim(),
                emailProviderIndex);
        etUserEmail.setText(newEmailText);
        setCaretBeforeAt();
    }


    ///////////////////////////////////////////////////////
    ////    email handling
    ///////////////////////////////////////////////////////

    private String appendEmailProvider(String emailText, int emailProviderIndex)
    {
        int foundAtAt = emailText.indexOf("@");
        if (foundAtAt == -1)
        {
            return emailText + emailProviders[emailProviderIndex];
        }
        else
        {
            return emailText.substring(0, foundAtAt) + emailProviders[emailProviderIndex];
        }
    }

    private void setCaretBeforeAt()
    {
        String emailText = etUserEmail.getText().toString();
        int foundAtAt = emailText.indexOf("@");
        if (foundAtAt == -1)
        {
            etUserEmail.setSelection(0);
        }
        else
        {
            etUserEmail.setSelection(foundAtAt);
        }
    }


}
